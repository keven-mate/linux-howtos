# Linux HowTos

In this repo I will be posting some explanations on how to do certain things in Linux, I won't be focued on a single matter, instead I will post about things that are actually hard to find explanations for or the wikis are just too difficult to understand.

As a newbie Linux user, I don't understand a lot of terminology and I don't know how to write Shell Scrips on my own ("yet"), I will do my best to avoid any complicated terminology and hard to swallow explanations with lack of better description on how to actualy fix the issue...

I will separate topics by their types, for example, *Window Managers* topics will be separate of *Init System* topics are so forth.

***Beware***: I'm no possible way a __Linux Power User__ (well, I may be on my way to be one), so don't expect my explanations to satisfy you if you consider yourself a power user.

I will be linking links to README's located in ***src/topic/title***, like for example, __src/Window_Managers/Apply_Qt_Themes_to_GUI_programs__ I will be following this scheme.

We all know that to survive on Linux we need to fix something (or break, or let someone break for us), that's why we love Linux. There's always something that needs a fix even in the most innapropiate time, like when you're at work and suddenly your video driver starts giving you *segmenation fault errors*, or maybe your network manager simply does not let you connect to the internet (just in spite). We Still LOVE LINUX.

***Note***: English is not my _default_ language, I'm a Portuguese speaker 😁 so don't expect my English to be perfect or fit your standards.

---

## Topics

### 1. Init Systems

<ul>
<li>1.1. [How To Write a Runit Supervised Service](https://gitlab.com/keven-mate/linux-howtos/-/blob/main/src/Init_Systems/Write_runit_supervised_service.md)</li>
</ul>
### 2. Window Managers
### 3. Linux Errors
